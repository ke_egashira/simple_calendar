<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title>calendar login</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/sub.css" />" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">

<link href="<c:url value="/resources/fullcalendar/core/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/fullcalendar/daygrid/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/fullcalendar/timegrid/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/fullcalendar/list/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/fullcalendar.css" />"
	rel="stylesheet">

<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/core/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/daygrid/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/interaction/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/timegrid/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/list/main.js" />"></script>

<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/core/locales/ja.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/fullcalendar.js" />"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/script.js" />"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/validation.js" />"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/ja.js"></script>

</head>
<body>
		<div class="header">
		<div id="icon">
			<a href="${pageContext.request.contextPath}/index"><img
				src="<c:url value="/resources/image/calendar-icon.png" />"
				width="auto" height="30px" ></a>
			<div class="select-open-button">
				<input type="button" class="fas fa-chevron-down select-open-button"
					value="&#xf078; " />
			</div>
		</div>
		<div id="select-menu">
			<ul>
				<li><a href="${pageContext.request.contextPath}/index"> カレンダー</a>
				<li><a href="${pageContext.request.contextPath}/management"> アカウント一覧</a>
				<li><a href="${pageContext.request.contextPath}/signup"> アカウント作成</a>
				<li><a href="${pageContext.request.contextPath}/logout"> ログアウト</a>
			</ul>
		</div>
	</div>
	<div id="main">
		<div id="main-content">
		<label id="submenu-label"><i class="fas fa-sign-in-alt"></i> ログイン</label>
			<div id="main-content-body">
				<label>アカウント名</label>
				<p><input id="input-textarea" /></p>
				<label>パスワード</label>
				<p><input type="password" id="input-textarea" /></p>
				<input type="submit" id="login-submit" value="ログイン">
			</div>
		</div>
	</div>
	<footer>
		<p><small>Copyright © Tadano Calendar. All rights reserved</small></p>
	</footer>
</body>
</html>
