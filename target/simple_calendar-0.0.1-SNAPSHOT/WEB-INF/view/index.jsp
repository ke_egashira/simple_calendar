<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title>calendar demo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">

<link href="<c:url value="/resources/fullcalendar/core/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/fullcalendar/daygrid/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/fullcalendar/timegrid/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/fullcalendar/list/main.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/fullcalendar.css" />"
	rel="stylesheet">

<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/core/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/daygrid/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/interaction/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/timegrid/main.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/list/main.js" />"></script>

<script type="text/javascript"
	src="<c:url value="/resources/fullcalendar/core/locales/ja.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/fullcalendar.js" />"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/script.js" />"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/validation.js" />"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/ja.js"></script>

</head>
<body>
	<div class="header">
		<div id="icon">
			<a href="${pageContext.request.contextPath}/index"><img
				src="<c:url value="/resources/image/calendar-icon.png" />"
				width="auto" height="30px" ></a>
			<div class="select-open-button">
				<input type="button" class="fas fa-chevron-down select-open-button"
					value="&#xf078; " />
			</div>
		</div>
		<div id="select-menu">
			<ul>
				<li><a href="${pageContext.request.contextPath}/index"> カレンダー</a>
				<li><a href="${pageContext.request.contextPath}/management"> アカウント一覧</a>
				<li><a href="${pageContext.request.contextPath}/signup"> アカウント作成</a>
				<li><a href="${pageContext.request.contextPath}/logout"> ログアウト</a>
			</ul>
		</div>
	</div>
	<div id="main">
		<div id="submenu">
		<label id="submenu-label"><i class="far fa-calendar-plus"></i> 予定の追加</label>
			<form:form modelAttribute="eventForm" autocomplete="off"
				action="${pageContext.request.contextPath}/event/insert">
				<div id="submenu-body">
				<div id="validation-message">
							<p id="error=message"><i class="far fa-angry"></i> 入力に不正があります</p>
						</div>
					<div class=allday-checkbox>
						<p>
							終日: <input type="checkbox" name="allDay" value="checked" id="checkbox" />
						</p>
					</div>
						<p id="errors">
							<form:errors path="title" id="errors" />
						</p>
					<p>予定: <form:input path="title" class="input-text" placeholder="タイトル" /></p>
					<div class="settime-inputbox">
						<p>
							開始:
							<form:input path="startDate" type="date" id="input-date-start" value="${today}" />
							<form:input path="startTime" type="time" id="input-time-start" value="${now}" />
						</p>
						<p>
							終了:
							<form:input path="endDate" type="date" id="input-date-end" value="${today}" />
							<form:input path="endTime" type="time" id="input-time-end" value="${now}" />
						</p>
					</div>
					<div class="setDate-inputbox">
					<p>
						開始:
						<form:input path="start" type="date" id="allDay-input-date" value="${today}" />
					</p>
					<p>
						終了:
						<form:input path="end" type="date" id="allDay-input-date-end" value="${today}" />
					</p>
					</div>
					<form:input type="hidden" path="userId" value="1" />
					<p><input type="submit" class="event-submit" value="登録する" disabled /></p>
				</div>
			</form:form>
		</div>
		<div id="main-content-wrapper">
			<div id="calendar"></div>
		</div>
	</div>

	<!-- モーダルウィンドの表示 -->
	<div id="modalwindow-wrapper">
	<div id="overlay"></div>
	<div id="edit-event-modalwindow">
	<div id="modal-validation-message">
		<p id="error=message">
			<i class="far fa-angry"></i> 入力に不正があります
		</p>
	</div>
	<div id="modal-close-button"><i class="far fa-times-circle" ></i></div>
	<form:form modelAttribute="eventForm" autocomplete="off"
				action="${pageContext.request.contextPath}/event/update">
			<p>
				終日: <input type="checkbox" name="modal-allDay" value="checked" id="modal-checkbox"  />
			</p>
			<p>予定: <form:input path="title" name="title" id="modal-input-text" placeholder="タイトル" />
		</p>
				<div id="modal-settime-inputbox">
					<p>
						開始:
						<form:input path="startDate" type="date" id="modal-input-date-start" />
						<form:input path="startTime" type="time" id="modal-input-time-start" />
					</p>
					<p>
						終了:
						<form:input path="endDate" type="date" id="modal-input-date-end" />
						<form:input path="endTime" type="time" id="modal-input-time-end" />
					</p>
				</div>
				<p>
				開始:
				<form:input path="start" type="date" id="modal-allDay-input-date" />
			</p>
			<p>
				終了:
				<form:input path="end" type="date" id="modal-allDay-input-date-end" />
			</p>

		<form:input path="id" type="hidden" id="modal-eventid" />
		<input type="submit" class="edit-event-submit" value="変更する" />
		</form:form>

		<form:form modelAttribute="eventForm" autocomplete="off"
				action="${pageContext.request.contextPath}/event/delete">
				<form:input path="id" type="hidden" id="modal-delete-eventid" />
		<input type="submit" id="delete-event-submit" value="予定を削除" />
		</form:form>
	</div>
	</div>
	<footer>
		<p><small>Copyright © Tadano Calendar. All rights reserved</small></p>
	</footer>


</body>
</html>
