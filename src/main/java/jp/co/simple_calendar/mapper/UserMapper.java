package jp.co.simple_calendar.mapper;

import jp.co.simple_calendar.entity.User;

public interface UserMapper {

	void insertUser(User entity);

	User getUserByAccount(String account);

	void updateUser(User entity);

}
