package jp.co.simple_calendar.mapper;

import java.util.List;

import jp.co.simple_calendar.entity.Event;

public interface EventMapper {

    List<Event>  getEventAll();

    void insertEvent(Event entity);

    void deleteEvent(int id);

    void updateEvent(Event entity);

}
