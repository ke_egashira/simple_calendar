package jp.co.simple_calendar.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import jp.co.simple_calendar.dto.EventDto;
import jp.co.simple_calendar.entity.Event;
import jp.co.simple_calendar.form.EventForm;
import jp.co.simple_calendar.mapper.EventMapper;

@Service
public class EventService {

	@Autowired
	private EventMapper eventMapper;

	public List<EventDto> getEventAll() {
		List<Event> eventList = eventMapper.getEventAll();
		List<EventDto> resultList = convertToDto(eventList);
		return resultList;
	}

	private List<EventDto> convertToDto(List<Event> eventList) {
		List<EventDto> resultList = new LinkedList<>();
		for (Event entity : eventList) {
			EventDto dto = new EventDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	@Transactional
	public void insertEvent(EventForm eventForm) {

		if (StringUtils.isEmpty(eventForm.getStart())) {
		String start = eventForm.getStartDate() + " " + eventForm.getStartTime();
		eventForm.setStart(start);

		String end = eventForm.getEndDate() + " " + eventForm.getEndTime();
		eventForm.setEnd(end);

		}

		EventDto dto = new EventDto();
		Event entity = new Event();
		BeanUtils.copyProperties(eventForm, dto);
		BeanUtils.copyProperties(dto, entity);
		eventMapper.insertEvent(entity);
	}

	@Transactional
	public void deleteEvent(EventForm form) {
		int id = form.getId();
		eventMapper.deleteEvent(id);
	}

	@Transactional
	public void updateEvent(EventForm eventForm) {

		if (StringUtils.isEmpty(eventForm.getStart())) {
		String start = eventForm.getStartDate() + " " + eventForm.getStartTime();
		eventForm.setStart(start);

		String end = eventForm.getEndDate() + " " + eventForm.getEndTime();
		eventForm.setEnd(end);

		}
		System.out.println("えらーじゃないですservice" + eventForm.getId());

		EventDto dto = new EventDto();
		Event entity = new Event();
		BeanUtils.copyProperties(eventForm, dto);
		BeanUtils.copyProperties(dto, entity);
		eventMapper.updateEvent(entity);
	}
}
