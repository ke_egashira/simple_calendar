package jp.co.simple_calendar.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.simple_calendar.dto.UserDto;
import jp.co.simple_calendar.entity.User;
import jp.co.simple_calendar.form.UserForm;
import jp.co.simple_calendar.mapper.UserMapper;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Transactional
	public void insertUser(UserForm userForm) {

		String encordPassword = passwordEncoder.encode(userForm.getPassword());
		userForm.setPassword(encordPassword);


		UserDto dto = new UserDto();
		User entity = new User();
		BeanUtils.copyProperties(userForm, dto);
		BeanUtils.copyProperties(dto, entity);
		userMapper.insertUser(entity);
	}

	@Transactional
	public void updateUser(UserForm userForm) {

		String getPassword = userForm.getPassword();

		if (getPassword != null) {
			String encordPassword = passwordEncoder.encode(getPassword);
			userForm.setPassword(encordPassword);
			System.out.println("パスワード入力されてます！");
		}

		System.out.println("うんこ！");
		UserDto dto = new UserDto();
		User entity = new User();
		BeanUtils.copyProperties(userForm, dto);
		BeanUtils.copyProperties(dto, entity);
		userMapper.updateUser(entity);
	}

	public UserDto authUser(UserForm form) {

		UserDto dto = new UserDto();
		User entity = userMapper.getUserByAccount(form.getAccount());

		if(entity == null) {
			return null;
		}

		BeanUtils.copyProperties(entity, dto);

		if( ! passwordEncoder.matches(form.getPassword(), dto.getPassword())) {
			return null;
		}

		return dto;
	}

}
