package jp.co.simple_calendar.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simple_calendar.form.UserForm;
import jp.co.simple_calendar.service.UserService;

@Controller
public class ManagementController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/management", method = RequestMethod.GET)
	public String management(Model model) {
		UserForm form = new UserForm();
		model.addAttribute("userForm", form);
		return "management";

	}

	@RequestMapping(value = "/user/update", method = RequestMethod.POST)
	public String userUpdate(@Valid @ModelAttribute UserForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "management";
		} else {
			userService.updateUser(form);
			return "redirect:/index";
		}
	}

}
