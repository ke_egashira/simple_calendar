package jp.co.simple_calendar.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simple_calendar.form.EventForm;
import jp.co.simple_calendar.service.EventService;


@Controller
public class CalendarController {

	@Autowired
	private EventService eventService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		EventForm form = new EventForm();
		model.addAttribute("eventForm", form);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		SimpleDateFormat onlyDayFormat = new SimpleDateFormat("dd");
		Date date = new Date();
		String today = (dateFormat.format(date));
		String nowTime = (timeFormat.format(date));
		String onlyDay = (onlyDayFormat.format(date));

		model.addAttribute("today", today);
		model.addAttribute("now", nowTime);
		model.addAttribute("iconDate", onlyDay);

		return "index";
	}

	@RequestMapping(value = "/event/insert", method = RequestMethod.POST)
	public String eventInsert(@Valid @ModelAttribute EventForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "index";
		} else {
			eventService.insertEvent(form);
			return "redirect:/index";
		}
	}

	@RequestMapping(value = "/event/delete", method = RequestMethod.POST)
	public String eventDelete(@ModelAttribute EventForm form, Model model) {
		eventService.deleteEvent(form);
		return "redirect:/index";

	}

	@RequestMapping(value = "/event/update", method = RequestMethod.POST)
	public String eventUpdate(@Valid @ModelAttribute EventForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "index";
		} else {
			eventService.updateEvent(form);
			System.out.println("えらーじゃないです");
			return "redirect:/index";
		}
	}
}
