package jp.co.simple_calendar.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simple_calendar.dto.UserDto;
import jp.co.simple_calendar.form.UserForm;
import jp.co.simple_calendar.service.UserService;

@Controller
public class LoginController {

	@Autowired
	private HttpSession session;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		UserForm form = new UserForm();
		model.addAttribute("userForm", form);
		return "login";

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model) {
		session.invalidate();
		return "redirect:login";
	}

	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public String authUser(@Valid @ModelAttribute UserForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			form.setPassword("");
			return "login";
		} else {
			UserDto dto = userService.authUser(form);
			session.setAttribute("loginUser", dto);
			System.out.println("えらーじゃないです");
			return "redirect:/index";
		}
	}
}
