package jp.co.simple_calendar.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simple_calendar.form.UserForm;
import jp.co.simple_calendar.service.UserService;

@Controller
public class SignupController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String logout(Model model) {
		UserForm form = new UserForm();
		model.addAttribute("userForm", form);
		return "signup";
	}

	@RequestMapping(value = "/user/insert", method = RequestMethod.POST)
	public String userInsert(@Valid @ModelAttribute UserForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "signup";
		} else {
			userService.insertUser(form);
			return "redirect:/login";
		}
	}
}
