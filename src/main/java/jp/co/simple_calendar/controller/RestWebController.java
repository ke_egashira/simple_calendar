package jp.co.simple_calendar.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.co.simple_calendar.dto.EventDto;
import jp.co.simple_calendar.service.EventService;


@RestController
@RequestMapping(value = "/api/event", method = RequestMethod.GET, produces="application/json;charset=UTF-8")
public class RestWebController {

	@Autowired
	private EventService eventService;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces="application/json;charset=UTF-8")
    public String getEvents() {
        String jsonMsg = null;
        try {
            List<EventDto> events = eventService.getEventAll();

            // FullCalendarにエンコード済み文字列を渡す
            ObjectMapper mapper = new ObjectMapper();
            jsonMsg =  mapper.writerWithDefaultPrettyPrinter().writeValueAsString(events);

        } catch (IOException ioex) {
            System.out.println(ioex.getMessage());
        }
System.out.println(jsonMsg);
        return jsonMsg;
    }
}