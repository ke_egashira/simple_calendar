package jp.co.simple_calendar.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import jp.co.simple_calendar.dto.UserDto;

@WebFilter("/*")
@Component
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpRes = (HttpServletResponse) response;

		String errorMessage = "セッションが無効です。再ログインしてください。";

		UserDto user = (UserDto) session.getAttribute("loginUser");

		if(!isLoginAccess(httpReq)) {

			if (user == null) {
				session.setAttribute("errorMessage", errorMessage);
				httpRes.sendRedirect("login");
				return;
			}
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

	private boolean isLoginAccess(HttpServletRequest request) {

		String servletPath = request.getServletPath();
		if ( ! servletPath.matches("/.*login") &&
				! servletPath.endsWith(".css") &&
				! servletPath.endsWith(".png") &&
				! servletPath.endsWith(".js")){

			return false;
		}
		return true;

	}

}
