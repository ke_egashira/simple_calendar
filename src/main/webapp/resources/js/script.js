//画面読み込み時に新規作成の終日にチェックを予め付与
$(function() {
	if (!$("#checkbox:checked").val()) {
		$('#checkbox').prop('checked', true);
	}
});


$(function() {
	var i = new Date();
	var myDate = i.getDate();
	$('#icon-date').text(myDate);
});

$(function() {
	$('#select-menu').hide();
	$('.select-open-button').click(function() {
	    $('#select-menu').slideToggle('fast');
	    return false;
	});
});
$(function() {
	$(document).click(function() {
		$('.select-open-button').removeClass("open");
		$('#select-menu').slideUp('fast');
	});
});

$(function() {
	locateCenter();
	$(window).resize(locateCenter);

	function locateCenter() {
		let w = $(window).width();
		let h = $(window).height();

		let cw = $('#edit-event-modalwindow').outerWidth();
		let ch = $('#edit-event-modalwindow').outerHeight();

		$('#edit-event-modalwindow').css({
			'left' : ((w - cw) / 2) + 'px',
			'top' : ((h - ch) / 2) + 'px'
		});
	}
});

