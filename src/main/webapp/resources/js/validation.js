
//新規登録の終日チェック確認および時間設定エリアの表示切り替え
$(function() {
	$('input[id="checkbox"]').change(function() {
		if (!$("#checkbox:checked").val()) {
			$('.settime-inputbox').slideDown(200);
			$('#allDay-input-date, #allDay-input-date-end').addClass("close").prop('disabled', true);
			$('#input-date-start, #input-time-start, #input-date-end, #input-time-end').prop('disabled', false);
		} else {
			$('.settime-inputbox').slideUp(200);
			$('#allDay-input-date, #allDay-input-date-end').removeClass("close").prop('disabled', false);
			$('#input-date-start, #input-time-start, #input-date-end, #input-time-end').prop('disabled', true);

			var now = new Date()
			//チェックをつけたタイミングで現在時刻をHH:mm形式(テンプレートリテラル使用)で付与
			var nowTimeFmt = `${now.getHours()}:${now.getMinutes().toString().padStart(2, '0')}`;
			$('#input-time-start').val(nowTimeFmt);
			$('#input-time-end').val(nowTimeFmt);

		}
	});
});

//モーダルウィンドウ(編集削除)の終日チェック確認および時間設定エリアの表示切り替え
$(function() {
	$('input[id="modal-checkbox"]').change(function() {
		if (!$("#modal-checkbox:checked").val()) {
			$('#modal-settime-inputbox').slideDown(200);
		} else {
			$('#modal-settime-inputbox').slideUp(200);
			$('#modal-input-time-start').val('');
			$('#modal-input-time-end').val('');
		}
	});
});

//モーダルウィンドウ(編集削除)の終日チェック確認およびinputboxへのdisable付与、css用のクラス付与
$(function() {
	$('input[id="modal-checkbox"]').change(function() {
		var prop = $('#modal-allDay-input-date').prop('disabled');
		if (prop) {
			$('#modal-allDay-input-date, #modal-allDay-input-date-end').prop('disabled', false);
			$('#modal-allDay-input-date, #modal-allDay-input-date-end').removeClass("close");
			$('#modal-input-date-start, #modal-input-time-start, #modal-input-date-end, #modal-input-time-end').prop('disabled', true);
			$('#modal-input-date-start, #modal-input-time-start, #modal-input-date-end, #modal-input-time-end').addClass("close");
		} else {
			$('#modal-allDay-input-date, #modal-allDay-input-date-end').prop('disabled', true);
			$('#modal-allDay-input-date, #modal-allDay-input-date-end').addClass("close");
			$('#modal-input-date-start, #modal-input-time-start, #modal-input-date-end, #modal-input-time-end').prop('disabled', false);
			$('#modal-input-date-start, #modal-input-time-start, #modal-input-date-end, #modal-input-time-end').removeClass("close");
		}

	});
});

//新規登録時の日時表示エリアの差分回収
$(function() {
	$('#allDay-input-date').change(function() {
		var getStartData = $('#allDay-input-date').val();
		var getEndData = $('#allDay-input-date-end').val();
		$('#input-date-start').val(getStartData);
		$('#input-date-end').val(getEndData);
	});
});

$(function() {
	$('#allDay-input-date-end').change(function() {
		var getStartData = $('#allDay-input-date').val();
		var getEndData = $('#allDay-input-date-end').val();
		$('#input-date-start').val(getStartData);
		$('#input-date-end').val(getEndData);
	});
});

$(function() {
	$('#input-date-start').change(function() {
		var getStartDate = $('#input-date-start').val();
		var getEndDate = $('#input-date-end').val();
		$('#allDay-input-date').val(getStartDate);
		$('#allDay-input-date-end').val(getEndDate);
	});
});

$(function() {
	$('#input-date-end').change(function() {
		var getStartDate = $('#input-date-start').val();
		var getEndDate = $('#input-date-end').val();
		$('#allDay-input-date').val(getStartDate);
		$('#allDay-input-date-end').val(getEndDate);
	});
});

$(function() {
	$('#input-time-start').change(function() {
		var getTime = $('#input-time-start').val();
		$('#input-time-end').val(getTime);
	});
});

//モーダルウィンドウ内での日時表示エリアの差分回収
$(function() {
	$('#modal-allDay-input-date').change(function() {
		var getData = $('#modal-allDay-input-date').val();
		$('#modal-input-date-start').val(getData);
	});
});

$(function() {
	$('#modal-allDay-input-date-end').change(function() {
		var getData = $('#modal-allDay-input-date-end').val();
		$('#modal-input-date-end').val(getData);
	});
});

$(function() {
	$('#modal-input-date-start').change(function() {
		var getData = $('#modal-input-date-start').val();
		$('#modal-allDay-input-date').val(getData);
	});
});

$(function() {
	$('#modal-input-date-end').change(function() {
		var getData = $('#modal-input-date-end').val();
		$('#modal-allDay-input-date-end').val(getData);
	});
});

//新規作成タイトル入力時の字数確認(0-30)、日付の入力確認(yyyy-MM-dd)とsubmitの無効化
$(function() {
	$('.input-text').on('input', function() {
		var datePattern = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
		var count = $('.input-text').val().length;
		var startDate = $('#allDay-input-date').val();
		var endDate = $('#allDay-input-date-end').val();

		var ckpt1 = startDate.match(datePattern);
		var ckpt2 = endDate.match(datePattern);
		var getStartTime = $('#input-time-start').val();
		var getEndTime = $('#input-time-end').val();
		var compareStartDate = startDate.slice( 0,4 ) + startDate.substr( 5,2 ) + startDate.slice( -2 );
		var compareEndDate = endDate.slice( 0,4 ) + endDate.substr( 5,2 ) + endDate.slice( -2 );

		var startTime = getStartTime.slice( 0, 2 ) + getStartTime.slice( -2 );
		var endTime = getEndTime.slice( 0, 2 ) + getEndTime.slice( -2 );


		if ((count > 0 && 30 > count) && ckpt1 && ckpt2 &&
				((compareEndDate == compareStartDate) && (startTime <= endTime) ||
						(compareEndDate > compareStartDate))) {
			$('.event-submit').prop('disabled', false);
			$('.event-submit').addClass("notblank");
			$('#validation-message').addClass("close");
		} else {
			$('.event-submit').prop('disabled', true);
			$('.event-submit').removeClass("notblank");
			$('#validation-message').removeClass("close");
		}
	});
});

//新規作成日付入力時のタイトル字数確認(0-30)、日付の入力確認(yyyy-MM-dd)とsubmitの無効化
$(function() {
	$('#validation-message').addClass("close");
	$('#input-date-start, #input-date-end, #allDay-input-date, #allDay-input-date-end, #input-time-start, #input-time-end, #checkbox').on('change', function() {
		var datePattern = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
		var count = $('.input-text').val().length;
		var startDate = $('#input-date-start').val();
		var endDate = $('#input-date-end').val();
		var ckpt1 = startDate.match(datePattern);
		var ckpt2 = endDate.match(datePattern);

		var compareStartDate = startDate.slice( 0,4 ) + startDate.substr( 5,2 ) + startDate.slice( -2 );
		var compareEndDate = endDate.slice( 0,4 ) + endDate.substr( 5,2 ) + endDate.slice( -2 );

		var getStartTime = $('#input-time-start').val();
		var getEndTime = $('#input-time-end').val();

		var startTime = getStartTime.slice( 0, 2 ) + getStartTime.slice( -2 );
		var endTime = getEndTime.slice( 0, 2 ) + getEndTime.slice( -2 );

		if ((count > 0 && 30 > count) && ckpt1 && ckpt2 &&
				((compareEndDate == compareStartDate) && (startTime <= endTime) ||
						(compareEndDate > compareStartDate))) {
			$('.event-submit').prop('disabled', false);
			$('.event-submit').addClass("notblank");
			$('#validation-message').addClass("close");
		} else {
			$('.event-submit').prop('disabled', true);
			$('.event-submit').removeClass("notblank");
			$('#validation-message').removeClass("close");
		}
	});
});
