document.addEventListener('DOMContentLoaded', function() {
	var initialLocaleCode = 'en';
	var localeSelectorEl = document.getElementById('locale-selector');
	var calendarEl = document.getElementById('calendar');

	var date_now = new Date();
	var date_start = new Date(date_now.getFullYear(), date_now.getMonth(), 1);
	var date_end = new Date(date_now.getFullYear(), date_now.getMonth(), 1);
	var days = [ "日", "月", "火", "水", "木", "金", "土" ];
	date_end.setMonth(date_end.getMonth() + 12);

	var calendar = new FullCalendar.Calendar(calendarEl, {
		plugins : [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
		},


		//イベントのクリック時の処理
		eventClick : function(obj) {

			var startDate = moment(obj.event.start).format('YYYY-MM-DD');
			var startTime = moment(obj.event.start).format('HH:mm');
			var endDate = moment(obj.event.end).format('YYYY-MM-DD');
			var endTime = moment(obj.event.end).format('HH:mm');
			var title = obj.event.title;
			var id = obj.event.id;
			$('#modal-validation-message').addClass("close");

			if (startTime !== "00:00") {
				$("#modal-input-date-start").val(startDate).removeClass("close").prop("disabled", false);
				$('#modal-input-time-start').val(startTime).removeClass("close").prop("disabled", false);
				$("#modal-input-date-end").val(endDate).removeClass("close").prop("disabled", false);
				$("#modal-input-time-end").val(endTime).removeClass("close").prop("disabled", false);
				$('input[id="modal-allDay-input-date"]').val("").prop("disabled", true).addClass("close");
				$('input[id="modal-allDay-input-date-end"]').val("").prop("disabled", true).addClass("close");
				$('input[name="modal-allDay"]').prop('checked', false);

			} else {
				$('input[id="modal-input-date-start"]').val("").prop("disabled", true).addClass("close");
				$('input[id="modal-input-time-start"]').val("").prop("disabled", true).addClass("close");
				$('input[id="modal-input-date-end"]').val("").prop("disabled", true).addClass("close");
				$('input[id="modal-input-time-end"]').val("").prop("disabled", true).addClass("close");
				$('input[id="modal-allDay-input-date"]').val(startDate).removeClass("close").prop("disabled", false);
				if(endDate !== "Invalid date") {
					$('input[id="modal-allDay-input-date-end"]').val(endDate).removeClass("close").prop("disabled", false);
				} else {
					$('input[id="modal-allDay-input-date-end"]').val(startDate).removeClass("close").prop("disabled", false);
				}
				$('input[name="modal-allDay"]').prop('checked', true);
			}
			$("#modal-input-text").val(title);
			$("#modal-eventid").val(id);
			$("#modal-delete-eventid").val(id);

			if ($("#modal-checkbox:checked").val()) {
				$('#modal-settime-inputbox').slideUp('fast');
			} else {
				$('#modal-settime-inputbox').fadeIn('fast');
			}

			$('#overlay, #edit-event-modalwindow, #modal-close-button').fadeIn('fast');
			$('.edit-event-submit').prop('disabled', true);
			$('.edit-event-submit').removeClass("notblank");


			$('#modal-input-text').on('input', function() {
				var datePattern = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
				var startDate = $('#modal-allDay-input-date').val();
				var endDate = $('#modal-allDay-input-date-end').val();
				var ckpt1 = startDate.match(datePattern);
				var ckpt2 = endDate.match(datePattern);
				var count = $('#modal-input-text').val().length;

				var getStartTime = $('#modal-input-time-start').val();
				var getEndTime = $('#modal-input-time-end').val();
				var compareStartDate = startDate.slice( 0,4 ) + startDate.substr( 5,2 ) + startDate.slice( -2 );
				var compareEndDate = endDate.slice( 0,4 ) + endDate.substr( 5,2 ) + endDate.slice( -2 );

				var startTime = getStartTime.slice( 0, 2 ) + getStartTime.slice( -2 );
				var endTime = getEndTime.slice( 0, 2 ) + getEndTime.slice( -2 );

				if ((count > 0 && 30 > count) && ckpt1 && ckpt2 &&
						((compareEndDate == compareStartDate) && (startTime <= endTime) ||
								(compareEndDate > compareStartDate))) {
					$('.edit-event-submit').prop('disabled', false);
					$('.edit-event-submit').addClass("notblank");
					$('#modal-validation-message').addClass("close");
				} else {
					$('.edit-event-submit').prop('disabled', true);
					$('.edit-event-submit').removeClass("notblank");
					$('#modal-validation-message').removeClass("close");
				}
			});

			$('#modal-input-date-start, #modal-input-date-end, #modal-allDay-input-date, #modal-allDay-input-date-end, #modal-input-time-start, #modal-input-time-end, #modal-checkbox').on('change', function() {
				var datePattern = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
				var count = $('#modal-input-text').val().length;
				var startDate = $('#modal-input-date-start').val();
				var endDate = $('#modal-input-date-end').val();
				var ckpt1 = startDate.match(datePattern);
				var ckpt2 = endDate.match(datePattern);
				var getStartTime = $('#modal-input-time-start').val();
				var getEndTime = $('#modal-input-time-end').val();
				var compareStartDate = startDate.slice( 0,4 ) + startDate.substr( 5,2 ) + startDate.slice( -2 );
				var compareEndDate = endDate.slice( 0,4 ) + endDate.substr( 5,2 ) + endDate.slice( -2 );

				var startTime = getStartTime.slice( 0, 2 ) + getStartTime.slice( -2 );
				var endTime = getEndTime.slice( 0, 2 ) + getEndTime.slice( -2 );

				if ((count > 0 && 30 > count) && ckpt1 && ckpt2 &&
						((compareEndDate == compareStartDate) && (startTime <= endTime) ||
								(compareEndDate > compareStartDate))) {
					$('.edit-event-submit').prop('disabled', false);
					$('.edit-event-submit').addClass("notblank");
					$('#modal-validation-message').addClass("close");
				} else {
					$('.edit-event-submit').prop('disabled', true);
					$('.edit-event-submit').removeClass("notblank");
					$('#modal-validation-message').removeClass("close");
				}
			});

			if (!$("#modal-checkbox:checked").val()) {
				var getStartDate = $('#modal-input-date-start').val();
				var getEndDate = $('#modal-input-date-end').val();
				$('#modal-allDay-input-date').val(getStartDate);
				$('#modal-allDay-input-date-end').val(getEndDate);
			} else {
				var getStartDate = $('#modal-allDay-input-date').val();
				var getEndDate = $('#modal-allDay-input-date-end').val();
				$('#modal-input-date-start').val(getStartDate);
				$('#modal-input-date-end').val(getEndDate);
			}

			$('#modal-close-button').on('click', function() {
				$('#overlay, #edit-event-modalwindow, #modal-close-button').fadeOut('fast');
			});

			// => モーダルウィンドウを中央配置するための初期値を設定
		    locateCenter();
		    $(window).resize(locateCenter);

		    function locateCenter() {
		        let w = $(window).width();
		        let h = $(window).height();

		        let cw = $('#edit-event-modalwindow').outerWidth();
		        let ch = $('#edit-event-modalwindow').outerHeight();

		        $('#edit-event-modalwindow').css({
		          'left': ((w - cw) / 2) + 'px',
		          'top': ((h - ch) / 2) + 'px'
		        });
		    }
		},


		locale : 'ja',

		minTime : "00:00:00",
		maxTime : "24:00:00",
		defaultTimedEventDuration : '10:00:00',
		slotDuration : '00:30:00',
		snapMinutes : 15,
		editable : false,
		droppable : false,
		dragRevertDuration : 500,
		selectable : false,
		selectHelper : true,
		scrollTime : '09:00:00',
		unselectAuto : false,
		selectMinDistance : 0,
		firstDay : 0,

		navLinks : true,
		businessHours : true,
		editable : false,
		slotLabelFormat : [ {
			month : 'long',
			year : 'numeric'
		},
		{
			weekday : 'long'
		},

		{
			hour : 'numeric',
			minute : '2-digit',
			omitZeroMinute : false,
			meridiem : false
		} ],

		events : {
			url : 'http://localhost:8080/simple_calendar/api/event/all'
		},
		eventTimeFormat : {
			hour : 'numeric',
			minute : '2-digit'
		},
		displayEventTime : true,
		displayEventEnd : {
			month : true,
			basicWeek : true,
			"default" : true
		},
		timeFormat : {
			agenda : 'H:mm{ - H:mm}'
		},
	});

	calendar.render();

	// build the locale selector's options
	calendar.getAvailableLocaleCodes().forEach(function(localeCode) {
		var optionEl = document.createElement('option');
		optionEl.value = localeCode;
		optionEl.selected = localeCode == initialLocaleCode;
		optionEl.innerText = localeCode;
		localeSelectorEl.appendChild(optionEl);
	});

	// when the selected option changes, dynamically change the calendar option
	localeSelectorEl.addEventListener('change', function() {
		if (this.value) {
			calendar.setOption('locale', this.value);
		}
	});
});
